<?php

namespace JyEle\Ele;

use ElemeOpenApi\Api\MsgNewService;
use ElemeOpenApi\Api\MessageService;

trait ISV
{
    /**
     * 获取推送失败的消息列表
     * https://open.shop.ele.me/openapi/apilist/eleme-msgNew/eleme-msgNew-getPushFailMsg
     *
     * @param string $app_id 应用id
     * @return bool
     */
    public function getPushFailMsg($app_id)
    {
        try {
            $msg_new_service            = new msgNewService((object)['access_token' => ''], $this->configClass);
            $msg_query_request          = [];
            $msg_query_request["appId"] = $app_id;
            $res                        = $msg_new_service->get_push_fail_msg($msg_query_request);
            return $this->objectToArray($res);
        } catch (\LogicException $e) {
            $this->setError(['code' => $e->getCode(), 'msg' => $e->getMessage()]);
            return false;
        }
    }
    
    /**
     * ISV通过该接口向平台确认已成功拉取消息
     * https://open.shop.ele.me/openapi/apilist/eleme-msgNew/eleme-msgNew-confirmPullMsg
     *
     * @param string $app_id 应用id
     * @param string $msg_id 消息id
     * @return bool
     */
    public function confirmPullMsg($app_id, $msg_id)
    {
        try {
            $msg_list_object          = [];
            $msg_list_object["id"]    = $msg_id;
            $msg_list_object["appId"] = $app_id;
            
            $msg_list   = [];
            $msg_list[] = $msg_list_object;
            
            $msg_confirm_request            = [];
            $msg_confirm_request["appId"]   = $app_id;
            $msg_confirm_request["msgList"] = $msg_list;
            
            $msg_new_service = new msgNewService((object)['access_token' => ''], $this->configClass);
            $res             = $msg_new_service->confirm_pull_msg($msg_confirm_request);
            return $this->objectToArray($res);
        } catch (\LogicException $e) {
            $this->setError(['code' => $e->getCode(), 'msg' => $e->getMessage()]);
            return false;
        }
    }
    
    /**
     * 获取未到达的推送消息
     * https://open.shop.ele.me/openapi/apilist/eleme-message/eleme-message-getNonReachedMessages
     *
     * @param string $app_id 应用id
     * @return bool
     */
    public function getNonReachedMessages($app_id)
    {
        try {
            $message_service = new messageService($this->token, $this->configClass);
            $res             = $message_service->get_non_reached_messages(intval($app_id));
            if (!$res) {
                return ['result' => [], 'success' => true];
            }
            return $this->objectToArray($res);
        } catch (\LogicException $e) {
            $this->setError(['code' => $e->getCode(), 'msg' => $e->getMessage()]);
            return false;
        }
    }
}
