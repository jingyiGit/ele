<?php

namespace JyEle\Ele;

use ElemeOpenApi\Config\Config;
use ElemeOpenApi\Api\ProductService;
use ElemeOpenApi\Api\ShopService;
use ElemeOpenApi\Api\OrderService;

class Ele
{
    use Auth;
    use Shop;
    use Good;
    use User;
    use Order;
    use Delivery;
    use ISV;
    
    protected $domainUrl;
    protected $config = [];
    protected $shop_id = 0;     // 店铺Id
    protected $token = null;    // 店铺Token
    protected $error = null;
    
    // SDK
    protected $configClass;     // SDK配置
    protected $shopService;     // 店铺类
    protected $productService;  // 商品类
    protected $orderService;    // 订单类
    
    public function __construct($config = null)
    {
        $this->config = $config;
        
        // 初始化SDK配置
        $this->configClass = new Config($this->config['key'], $this->config['secret'], $config['sandbox']);
    }
    
    public function setShopToken($shop_id = null, $token = null)
    {
        if ($shop_id) {
            $this->shop_id = intval($shop_id);
        }
        if ($token) {
            $this->token = (object)['access_token' => $token];
            
            // 重新初始化类
            $this->productService = new productService($this->token, $this->configClass);
            $this->shopService    = new ShopService($this->token, $this->configClass);
            $this->orderService   = new orderService($this->token, $this->configClass);
        }
    }
    
    /**
     * 对象转数组
     *
     * @param $array
     * @return array|mixed|null
     */
    private function objectToArray($array)
    {
        if (!$array) {
            return $array;
        }
        if (is_object($array)) {
            $array = (array)$array;
        }
        
        if (is_array($array)) {
            foreach ($array as $key => $value) {
                if (is_object($value)) {
                    $value = (array)$value;
                }
                
                if (is_array($value)) {
                    $array[$key] = $this->objectToArray($value);
                }
            }
        }
        return $array;
    }
    
    private function handleReturn($res)
    {
        if ($res['code'] == 0 && $res['apiMessage'] == null) {
            $this->error = null;
            return $res['result'];
        }
        $this->setError($res);
        return false;
    }
    
    private function _checkShopId()
    {
        if (!$this->shop_id) {
            $this->setError([
                'code' => 0,
                'msg'  => '请使用 setShopToken() 设置店铺Id',
            ]);
            return false;
        }
        return true;
    }
    
    public function getError()
    {
        return $this->error;
    }
    
    private function setError($error)
    {
        $this->error = $error;
        return false;
    }
}
