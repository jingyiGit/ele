<?php
// 商品
// https://open.shop.ele.me/openapi/documents/reference/fc2da5ae0d3b4e219e5dcf6e668a8087

namespace JyEle\Ele;

trait Good
{
    /**
     * 查询店铺商品分类
     */
    public function getGoodCategories()
    {
        if (!$this->_checkShopId()) {
            return false;
        }
        try {
            $res = $this->productService->get_shop_categories($this->shop_id);
            return $this->objectToArray($res);
        } catch (\LogicException $e) {
            $this->setError(['code' => $e->getCode(), 'msg' => $e->getMessage()]);
            return false;
        }
    }
    
    /**
     * 查询店铺商品分类，包含二级分类
     */
    public function getGoodCategoriesWithChildren()
    {
        if (!$this->_checkShopId()) {
            return false;
        }
        try {
            $res = $this->productService->get_shop_categories_with_children($this->shop_id);
            return $this->objectToArray($res);
        } catch (\LogicException $e) {
            $this->setError(['code' => $e->getCode(), 'msg' => $e->getMessage()]);
            return false;
        }
    }
    
    /**
     * 查询商品分类详情
     */
    public function getCategory($category_id)
    {
        if (!$this->_checkShopId()) {
            return false;
        }
        try {
            $res = $this->productService->get_category($category_id);
            return $this->objectToArray($res);
        } catch (\LogicException $e) {
            $this->setError(['code' => $e->getCode(), 'msg' => $e->getMessage()]);
            return false;
        }
    }
    
    /**
     * 获取商品详情
     *
     * @param int $good_id
     */
    public function getDood($good_id)
    {
        try {
            $res = $this->productService->get_item($good_id);
            return $this->objectToArray($res);
        } catch (Exception $e) {
            $this->setError(['code' => $e->getCode(), 'msg' => $e->getMessage()]);
            return false;
        }
    }
    
    /**
     * 批量查询商品详情
     *
     * @param array $good_ids
     */
    public function getDoods($good_ids)
    {
        try {
            $res = $this->productService->batch_get_items($good_ids);
            return $this->objectToArray($res);
        } catch (Exception $e) {
            $this->setError(['code' => $e->getCode(), 'msg' => $e->getMessage()]);
            return false;
        }
    }
    
    /**
     * 获取商品列表_按分页
     *
     * @param int $page  页码
     * @param int $limit 每页返回的数量
     */
    public function getGoodListByPage($page = 1, $limit = 20)
    {
        if (!$this->_checkShopId()) {
            return false;
        }
        try {
            $param = [
                'shopId' => $this->shop_id,
                'offset' => ($page - 1) * $limit,
                'limit'  => $limit,
            ];
            $res   = $this->productService->query_item_by_page($param);
            return $this->objectToArray($res);
        } catch (Exception $e) {
            $this->setError(['code' => $e->getCode(), 'msg' => $e->getMessage()]);
            return false;
        }
    }
    
    /**
     * 获取商品列表_按分类
     *
     * @param int $category_id 分类Id
     * @return array|false|mixed|null
     */
    public function getGoodListByCate($category_id)
    {
        try {
            $res = $this->productService->get_items_by_category_id_v2($category_id);
            return $this->objectToArray($res);
        } catch (Exception $e) {
            $this->setError(['code' => $e->getCode(), 'msg' => $e->getMessage()]);
            return false;
        }
    }
    
    /**
     * 查询配料组
     *
     * @param int $ingredientGroupId 配料组id
     * @return array|false|mixed|null
     */
    public function getIngredientGroup($ingredientGroupId)
    {
        try {
            $res = $this->productService->get_ingredient_group($ingredientGroupId);
            return $this->objectToArray($res);
        } catch (Exception $e) {
            $this->setError(['code' => $e->getCode(), 'msg' => $e->getMessage()]);
            return false;
        }
    }
}
