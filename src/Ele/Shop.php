<?php
// 店铺
// https://open.shop.ele.me/openapi/documents/reference/6d06fb9f63c846c8b068c4cf2f53a5be

namespace JyEle\Ele;


trait Shop
{
    /**
     * 取店铺信息
     */
    public function getShopInfo()
    {
        if (!$this->_checkShopId()) {
            return false;
        }
        try {
            $res = $this->shopService->get_shop($this->shop_id);
            return $this->objectToArray($res);
        } catch (\Exception $e) {
            $this->setError(['code' => $e->getCode(), 'msg' => $e->getMessage()]);
            return false;
        }
    }
    
    /**
     * 更新店铺信息
     * https://open.shop.ele.me/openapi/apilist/eleme-shop/eleme-shop-updateShop
     *
     * @param array $params
     */
    public function updateShopInfo($params)
    {
        if (!$this->_checkShopId()) {
            return false;
        }
        try {
            $res = $this->shopService->update_shop($this->shop_id, $params);
            return $this->objectToArray($res);
        } catch (\Exception $e) {
            $this->setError(['code' => $e->getCode(), 'msg' => $e->getMessage()]);
            return false;
        }
    }
    
    /**
     * 更新店铺基本信息
     * https://open.shop.ele.me/openapi/apilist/eleme-shop/eleme-shop-updateShopBasicInfo
     *
     * @param array $params
     * @return array|false|mixed|null
     */
    public function updateShopBasicInfo($params)
    {
        if (!$this->_checkShopId()) {
            return false;
        }
        try {
            $res = $this->shopService->update_shop_basic_info($this->shop_id, $params);
            return $this->objectToArray($res);
        } catch (\Exception $e) {
            $this->setError(['code' => $e->getCode(), 'msg' => $e->getMessage()]);
            return false;
        }
    }
    
    /**
     * 查询店铺IM状态
     *
     * @return array|false|mixed|null
     */
    public function getIMStatus()
    {
        if (!$this->_checkShopId()) {
            return false;
        }
        try {
            $res = $this->shopService->get_i_m_status($this->shop_id);
            return $this->objectToArray($res);
        } catch (\Exception $e) {
            $this->setError(['code' => $e->getCode(), 'msg' => $e->getMessage()]);
            return false;
        }
    }
    
    /**
     * 更新店铺IM开关状态
     *
     * @param int $status IM开关状态（0：关闭 1：开启）
     * @return array|false|mixed|null
     */
    public function updateIMStatus($status)
    {
        if (!$this->_checkShopId()) {
            return false;
        }
        try {
            $res = $this->shopService->update_i_m_status($this->shop_id, $status);
            return $this->objectToArray($res);
        } catch (\Exception $e) {
            $this->setError(['code' => $e->getCode(), 'msg' => $e->getMessage()]);
            return false;
        }
    }
}
