<?php
// 自配送商家
// https://open.shop.ele.me/openapi/documents/delivery

namespace JyEle\Ele;

trait Delivery
{
    /**
     * 提交配送信息
     * https://open.shop.ele.me/openapi/apilist/eleme-order/eleme-order-selfDeliveryStateSync
     *
     * @param $deliveryInfo
     * @return array|false|mixed|null
     */
    public function deliveryStateSync($deliveryInfo)
    {
        if (!$this->_checkShopId()) {
            return false;
        }
        $state_info = [
            'orderId'             => $deliveryInfo['orderId'],               // 订单id
            'distributorId'       => 201,                                    // 运送商类型,固定传201
            'state'               => $deliveryInfo['state'],                 // 配送状态
            'knight'              => [
                'id'    => $deliveryInfo['knight_id'],    // 骑手Id
                'phone' => $deliveryInfo['knight_phone'], // 骑手电话
                'name'  => $deliveryInfo['knight_name'],  //骑手名字
            ],
            'deliveryCompanyId'   => $deliveryInfo['deliveryCompanyId'] ?? 125,                //配送公司ID
            'deliveryCompanyName' => $deliveryInfo['deliveryCompanyName'] ?? '第三方平台配送', // 配送公司名称
        ];
        try {
            $res = $this->orderService->self_delivery_state_sync($this->shop_id, $state_info);
            return $this->objectToArray($res);
        } catch (\Exception $e) {
            $this->setError(['code' => $e->getCode(), 'msg' => $e->getMessage()]);
            return false;
        }
    }
    
    /**
     * 同步骑手位置信息
     * https://open.shop.ele.me/openapi/apilist/eleme-order/eleme-order-selfDeliveryLocationSync
     *
     * @param array $locationInfo
     * @return array|false|mixed|null
     */
    public function syncDeliveryLocation($locationInfo)
    {
        if (!$this->_checkShopId()) {
            return false;
        }
        $orderId = $locationInfo['orderId'];
        unset($locationInfo['orderId']);
        $data = array_merge(['utc' => time()], $locationInfo);
        if (!$data['altitude']) {
            $data['altitude'] = 120.34;
        }
        
        try {
            $res = $this->orderService->self_delivery_location_sync($this->shop_id, $orderId, $data);
            return $this->objectToArray($res);
        } catch (\Exception $e) {
            $this->setError(['code' => $e->getCode(), 'msg' => $e->getMessage()]);
            return false;
        }
    }
}
