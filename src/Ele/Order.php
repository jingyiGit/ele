<?php
// 订单，可查询90天内的订单
// https://open.shop.ele.me/openapi/apilist/eleme-order/eleme-order-getOrder

namespace JyEle\Ele;

use ElemeOpenApi\Api\MessageService;

trait Order
{
    /**
     * 获取订单信息
     *
     * @param string $order_id 订单ID
     * @return array|false|mixed|null
     */
    public function getOrder($order_id)
    {
        try {
            $res = $this->orderService->get_order($order_id);
            return $this->objectToArray($res);
        } catch (\Exception $e) {
            $this->setError(['code' => $e->getCode(), 'msg' => $e->getMessage()]);
            return false;
        }
    }
    
    /**
     * 获取订单信息_批量
     * https://open.shop.ele.me/openapi/apilist/eleme-order/eleme-order-mgetOrders
     *
     * @param array $order_ids 订单ID数组
     * @return array|false|mixed|null
     */
    public function getOrders($order_ids)
    {
        try {
            $res = $this->orderService->mget_orders($order_ids);
            return $this->objectToArray($res);
        } catch (\Exception $e) {
            $this->setError(['code' => $e->getCode(), 'msg' => $e->getMessage()]);
            return false;
        }
    }
    
    /**
     * 查询全部订单
     * https://open.shop.ele.me/openapi/apilist/eleme-order/eleme-order-getAllOrders
     *
     * @param int $page  页码
     * @param int $limit 每页返回的数量
     * @param int $date  某一天的10位时间戳，留空为当天
     */
    public function getOrderAll($page = 1, $limit = 20, $date = 0)
    {
        if (!$this->_checkShopId()) {
            return false;
        }
        if (!$date) {
            $date = date('Y-m-d', time());
        } else {
            $date = date('Y-m-d', $date);
        }
        try {
            $res = $this->orderService->get_all_orders($this->shop_id, $page, $limit, $date);
            return $this->objectToArray($res);
        } catch (\Exception $e) {
            return $this->setError(['code' => $e->getCode(), 'msg' => $e->getMessage()]);
        }
    }
    
    /**
     * 查询店铺未处理订单
     * https://open.shop.ele.me/openapi/apilist/eleme-order/eleme-order-getUnprocessOrders
     */
    public function getUnprocessOrders()
    {
        try {
            $res = $this->orderService->get_unprocess_orders($this->shop_id);
            if (!$res) {
                return ['result' => [], 'success' => true];
            }
            return $this->objectToArray($res);
        } catch (\Exception $e) {
            return $this->setError(['code' => $e->getCode(), 'msg' => $e->getMessage()]);
        }
    }
    
    /**
     * 帮商家接单
     *
     * @return bool
     */
    public function confirmOrder($order_id)
    {
        if (!$order_id) {
            return $this->setError('order_id 不能为空');
        }
        try {
            $this->orderService->confirm_order_lite($order_id);
            return true;
        } catch (\Exception $e) {
            return $this->setError(['code' => $e->getCode(), 'msg' => $e->getMessage()]);
        }
    }
    
    /**
     * 自配送商家，同步订单的状态信息
     * https://open.shop.ele.me/openapi/apilist/eleme-order/eleme-order-selfDeliveryStateSync
     *
     * @param array $order_info 订单信息
     * @param array $rider_info 可空，骑手信息
     * @return bool
     */
    public function delivery_state_sync($order_info, $rider_info = [])
    {
        if (!$order_info['order_id']) {
            return $this->setError('order_id 不能为空');
        }
        $state_info                        = [];
        $state_info["orderId"]             = $order_info['order_id'];
        $state_info["distributorId"]       = 201;
        $state_info["knight"]              = $rider_info;
        $state_info["state"]               = $order_info['state'] ?: 'DELIVERY_START';
        $state_info["deliveryCompanyId"]   = $order_info['deliveryCompanyId'] ?: 125125;
        $state_info["deliveryCompanyName"] = $order_info['deliveryCompanyName'] ?: '哪都达';
        try {
            $this->orderService->self_delivery_state_sync($this->shop_id, $state_info);
            return true;
        } catch (\Exception $e) {
            return $this->setError(['code' => $e->getCode(), 'msg' => $e->getMessage()]);
        }
    }
    
    /**
     * 自配送商家，同步订单的位置信息
     * https://open.shop.ele.me/openapi/apilist/eleme-order/eleme-order-selfDeliveryLocationSync
     *
     * @param string $order_id
     * @param array  $location_info
     * @return bool
     */
    public function delivery_location_sync($order_id, $location_info)
    {
        if (!$order_id) {
            return $this->setError('order_id 不能为空');
        }
        if (!isset($location_info['utc'])) {
            $location_info['utc'] = time();
        }
        if (!isset($location_info['altitude'])) {
            $location_info['altitude'] = '120.125';
        }
        try {
            $this->orderService->self_delivery_location_sync($this->shop_id, $order_id, $location_info);
            return true;
        } catch (\Exception $e) {
            return $this->setError(['code' => $e->getCode(), 'msg' => $e->getMessage()]);
        }
    }
    
    /**
     * 同步配送信息
     *
     * @remark delivery_state_sync 和 delivery_location_sync的组合
     * @param array $param
     * @return bool
     */
    public function riderPosition($param)
    {
        $order_info = [
            'order_id' => $param['order_id'],
            'state'    => $param['state'],
        ];
        if (isset($param['deliveryCompanyId'])) {
            $order_info['deliveryCompanyId'] = $param['deliveryCompanyId'];
        }
        if (isset($param['deliveryCompanyName'])) {
            $order_info['deliveryCompanyName'] = $param['deliveryCompanyName'];
        }
        $rider_info = [
            'id'    => $param['rider_id'] ?: 1,
            'name'  => $param['rider_name'],
            'phone' => $param['rider_phone'],
        ];
        
        // 同步订单的状态信息
        if (!$this->delivery_state_sync($order_info, $rider_info)) {
            return false;
        }
        
        // 同步订单的位置信息
        $location_info = [
            'longitude' => $param['longitude'],
            'latitude'  => $param['latitude'],
        ];
        if (isset($param['altitude'])) {
            $location_info['altitude'] = $param['altitude'];
        }
        if (isset($param['utc'])) {
            $location_info['utc'] = $param['utc'];
        }
        if (!$this->delivery_location_sync($param['order_id'], $location_info)) {
            return false;
        }
        return true;
    }
    
    /**
     * 禁止推送消息设置
     * https://open.shop.ele.me/openapi/apilist/eleme-message/eleme-message-disablePushConfig
     *
     * @param int    $bid           店铺ID
     * @param string $message_types 消息类型，商户接单(12),订单被取消(14),订单置为无效(15),订单强制无效(17),订单已完成(18),取消授权(100),订单生效(217)
     *                              留空表示全部开启
     *                              all:全部关闭(10,12,14,15,17,18,100,217)
     * @return bool
     */
    public function stopCallPush($bid, $message_types = '')
    {
        if (!$message_types) {
            $message_types = [];
        } else if ($message_types === 'all') {
            $message_types = ['10', '12', '14', '15', '17', '18', '100', '217'];
        } else {
            $message_types = is_array($message_types) ? $message_types : explode(',', $message_types);
        }
        
        $disable_push_request           = [];
        $disable_push_request["shopId"] = intval($bid);
        if ($message_types) {
            $disable_push_request["messageTypes"] = $message_types;
        }
        $message_service = new messageService($this->token, $this->configClass);
        $message_service->disable_push_config($disable_push_request);
        return true;
    }
    
    /**
     * 查询开发者绑定门店停止推送的消息列表
     * https://open.shop.ele.me/openapi/apilist/eleme-message/eleme-message-queryDisablePush
     *
     * @return array
     */
    public function getStopCallPushList($bid)
    {
        $disable_push_query             = [];
        $disable_push_query["shopId"]   = intval($bid);
        $disable_push_query["pageNo"]   = 1;
        $disable_push_query["pageSize"] = 20;
        
        $message_service = new messageService((object)['access_token' => ''], $this->configClass);
        $res             = $message_service->query_disable_push($disable_push_query);
        if ($temp = $this->objectToArray($res)) {
            if ($temp['result'][0]) {
                return $temp['result'][0]['messageTypes'] ?: [];
            }
            return $temp['result'];
        }
        return $this->objectToArray($res);
    }
}
