<?php
// 帐户
// https://open.shop.ele.me/openapi/apilist/eleme-user/eleme-user-getUser

namespace JyEle\Ele;

use ElemeOpenApi\Api\UserService;

trait User
{
  /**
   * 取商户账号信息
   */
  public function getUser()
  {
    $userService = new userService($this->token, $this->configClass);
    try {
      $res = $userService->get_user();
      return $this->objectToArray($res);
    } catch (Exception $e) {
      $this->setError(['code' => $e->getCode(), 'msg' => $e->getMessage()]);
      return false;
    }
  }
}
