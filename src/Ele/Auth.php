<?php
// 授权
// https://open.shop.ele.me/openapi/documents/isvoauth

namespace JyEle\Ele;

use ElemeOpenApi\OAuth\OAuthClient;

trait Auth
{
    /**
     * 自动判断，回调模式
     *
     * @param $params
     * @param $call
     */
    public function auto($params, $call)
    {
        if (!$call) {
            exit('call 必须提供');
        } elseif (!$params) {
            exit('params 必须提供');
        }
        
        // 创建并自动跳转到授权地址
        if (!isset($_GET['code'])) {
            header("Location: " . $this->createAuthUrl($params['redirect_uri'], $params['state']));
            exit();
        } elseif (isset($_GET['code'])) {
            $call && $call($this->getToken($params['redirect_uri'], trim($_GET['code'])));
        }
    }
    
    /**
     * 创建授权链接
     */
    public function createAuthUrl($redirect_uri, $state)
    {
        $client = new OAuthClient($this->configClass);
        return $client->get_auth_url($state, 'all', $redirect_uri);
    }
    
    /**
     * 获取Token
     *
     * @param string $redirect_uri
     * @param string $code
     */
    public function getToken($redirect_uri, $code)
    {
        try {
            $client = new OAuthClient($this->configClass);
            return (array)$client->get_token_by_code($code, $redirect_uri);
        } catch (\ElemeOpenApi\Exception\IllegalRequestException $e) {
            return ['code' => $e->getCode(), 'msg' => $e->getMessage()];
        }
    }
    
    /**
     * 刷新Token
     *
     * @param string $refresh_token
     */
    public function refreshToken($refresh_token)
    {
        try {
            $client = new OAuthClient($this->configClass);
            $res    = (array)$client->get_token_by_refresh_token($refresh_token, 'all');
            if ($res['code'] === 0) {
                $this->setError($res);
                return false;
            }
            return $res;
        } catch (\ElemeOpenApi\Exception\IllegalRequestException $e) {
            $this->setError(['code' => $e->getCode(), 'msg' => $e->getMessage()]);
            return false;
        }
    }
}
