<?php

namespace JyEle\Init;

use JyEle\Kernel\Response;
use JyEle\Ele\Ele;
use JyEle\BasicService\BaseConfig;

/**
 * Class Application.
 */
class Application extends Ele
{
    use Response;
    use BaseConfig;
    
    public function __construct(array $config = [])
    {
        parent::__construct($this->initConfig($config));
    }
}
